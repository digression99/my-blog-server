
const api = require('express').Router();
const postApi = require('./posts');

api.use('/posts', postApi);

module.exports = api;

