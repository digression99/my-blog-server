const mongoose = require('mongoose');
const router = require('express').Router();
const Post = mongoose.model('post');
const Hashtag = mongoose.model('hashtag');

const { getTagIds } = require('../lib');

const savePost = async (req, res) => {
    const {title, content, tags } = req.body;

    try {
        const post = new Post({ title, content });
        const hashtagsToSave = tags.map(tag => new Hashtag({
            tag,
            postId : post._id
        }));
        post.hashtags = hashtagsToSave.map(hashtag => hashtag._id);
        await post.save();
        hashtagsToSave.map(hashtag => hashtag.save());
        res.status(200).json({title, content});
    } catch (e) {
        console.log('error : ', e);
        res.status(400).send(e);
    }
};

const deletePost = async (req, res) => {
    const { postId } = req.body;

    try {
        const post = await Post.findById(postId).exec();

        await Hashtag.deleteMany({ postId });
        await Post.findByIdAndRemove(postId);

        res.status(200).json({
            postId
        });
    } catch (e) {
        console.log('error');
        console.log(e);
        res.status(500).json({
            error : e
        });
    }
};

const editPost = async (req, res) => {
    const { postId, title, content, tags } = req.body;

    try {
        const hashtagsToSave = tags.map(tag => new Hashtag({
            tag,
            postId
        }));

        await Hashtag.deleteMany({ postId });
        await Post.updateOne({ _id : postId }, {
            $set : {
                title,
                content,
                hashtags : hashtagsToSave.map(hashtag => hashtag._id),
                dateModified : new Date()
            }
        });

        hashtagsToSave.map(hashtag => hashtag.save());
        res.status(200).json({
            postId
        });
    } catch (e) {
        console.log('error in editPost');
        console.log(e);
        res.status(500).json({
            error : e
        });
    }

};

const getAllPosts = async (req, res) => {
    try {
        let posts = await Post.find({}).populate('hashtags').exec();
        res.status(200).json(posts);
    } catch (e) {
        console.log("error : ");
        console.log(e);
        res.status(400).json({
            error: e
        });
    }
};

module.exports = {
    savePost,
    getAllPosts,
    deletePost,
    editPost
};
