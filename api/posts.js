const router = require('express').Router();
const {
    savePost,
    getAllPosts,
    deletePost,
    editPost
} = require('./posts.ctrl');

router.post('/', savePost);
router.get('/', getAllPosts);
router.delete('/', deletePost);
router.patch('/', editPost);

module.exports = router;
