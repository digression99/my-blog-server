
require('./config');

const {
    NODE_ENV
} = process.env;

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');

const api = require('./api');

const app = express();

// logger setting.
app.use(logger('dev'));

// express setting.
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// body parser.
app.use(bodyParser.json());

// apis.
app.use('/api', api);
// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});


// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development

    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send('error');
    // res.render('error');
});

if (NODE_ENV === 'production') {
    app.get('*', (req, res) => {
        // send client build file.
        res.sendFile(path.join(__dirname, '../client/build/index.html'));
    })
} else if (NODE_ENV === 'development') {
    app.get('*', (req, res) => {
        res.send('Do not approach here.');
    });
}

module.exports = app;
