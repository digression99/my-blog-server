const {
    MONGODB_USERNAME,
    MONGODB_PASSWORD,
    MONGODB_URI
} = process.env;

const mongoose = require('mongoose');
require('../models/post');
require('../models/hashtag');

const url = `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_URI}`;

mongoose.connect(url).then(() => {
    console.log('successfully connected to mongodb.');
}, e => {
    console.log('error occured while connecting to mongodb.');
    console.log(e);
    return mongoose.disconnect();
});

// mongoose.connect(`mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_URI}`).