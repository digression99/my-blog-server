const mongoose = require('mongoose');
const Post = mongoose.model('post');
const Hashtag = mongoose.model('hashtag');

const getTagIds = (tags, postId) => new Promise(async (resolve, reject) => {
    try {
        const tagIdArray = tags.map(tag => {
            const hashtag = new Hashtag({ tag, postId });
            console.log('created hashta : ');
            console.log(JSON.stringify(hashtag, undefined, 2));

            hashtag.save(); // no need to wait.
            return hashtag._id;
        });
        resolve(tagIdArray);
    } catch (e) {
        console.log(e);
        reject(e);
    }
});

module.exports = {
    getTagIds
};