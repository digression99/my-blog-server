const mongoose = require('mongoose');
const { Schema } = mongoose;

const hashtagSchema = new Schema({
    tag : {
        type : String,
        required : true,
        trim : true
    },
    dateCreated : {
        type : Date,
        default : Date.now()
    },
    dateModified : {
        type : Date,
        default : Date.now()
    },
    postId : {
        type : Schema.Types.ObjectId,
        ref : 'post'
    }
});

module.exports = mongoose.model('hashtag', hashtagSchema);
