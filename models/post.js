const mongoose = require('mongoose');
const { Schema } = mongoose;

const postSchema = new Schema({
    title : {
        type : String,
        required : true,
        trim : true
    },
    content : {
        type : String,
        required : true,
        trim : true
    },
    dateCreated : {
        type : Date,
        default : Date.now()
    },
    dateModified : {
        type : Date,
        default : Date.now()
    },
    hashtags : {
        type : [{
            type : Schema.Types.ObjectId,
            ref : 'hashtag'
        }],
        default : []
    }
});

module.exports = mongoose.model('post', postSchema);